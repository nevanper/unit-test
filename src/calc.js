// TODO: arithmetic operations
/**
 * Adds two number together.
 * @param {number} a 
 * @param {number} b 
 * @returns {number} 
 */
const add = (a, b) => a + b;

/**
 * Subtracts one number from another.
 * @param {number}} minuend 
 * @param {number*} subtrahend 
 * @returns {number}
 */
const subtract = (minuend, subtrahend) => {
    return minuend - subtrahend;
};

/**
 * Multiplies a number with another.
 * @param {number} multiplier 
 * @param {number} multiplicant 
 * @returns {number}
 */
const multiply = (multiplier, multiplicant) => {
    return multiplier * multiplicant;
}

/**
 * Divides a number with another.
 * @param {number} dividend 
 * @param {number} divisor 
 * @returns {number}
 * @throws {Error}
 */
const divide = (dividend, divisor) => {
    if (divisor == 0) throw new Error("division by zero is not allowed");
    const fraction = dividend / divisor;
    return fraction;
}

export default { add, subtract, multiply, divide }